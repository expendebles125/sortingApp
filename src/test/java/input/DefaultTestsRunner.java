package input;


import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import static org.junit.Assert.assertEquals;

public class DefaultTestsRunner {
    @Test
    public void testIsValidInputString() {
        JUnitCore core = new JUnitCore();
        Result result = core.run(TestIsValidString.class);
        assertEquals(1,result.getFailureCount());
        assertEquals("input string is not an Integer", result.getFailures().get(0).getException().getMessage());
    }

    @Test
    public void testInputValues() {
        JUnitCore core = new JUnitCore();
        Result result = core.run(TestInputValues.class);
        assertEquals(1,result.getFailureCount());
        assertEquals("array size incorrect", result.getFailures().get(0).getException().getMessage());
    }
}
