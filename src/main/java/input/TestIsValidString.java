package input;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TestIsValidString {
    InputNumbers input = new InputNumbers();
    String inputString;

    public TestIsValidString(String s){
        this.inputString = s;
    }

    @Parameterized.Parameters
    public static String[] array(){
        return new String[]{
                "13","132","6","qewr"
        };
    }

    @Test
    public void testIsValid(){

        try{
            input.isValidInputString(inputString);
//            assertEquals(NumberFormatException.class, input.isValidInputString(inputString));
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("input string is not an Integer");
        }

    }
}
