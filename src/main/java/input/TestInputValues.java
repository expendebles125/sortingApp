package input;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TestInputValues {
    int[] currentArray;
    InputNumbers inputNumbers = new InputNumbers();

    public TestInputValues(int[] values){
        this.currentArray = values;
        InputNumbers.integerArrayList = new ArrayList<>();
    }

    @Parameterized.Parameters
    public static int[][] parArray(){
        return new int[][]{
                {0,2,3,1,4,5,6,7,8,9,10},
                {1,5,3,6},
                {0,2,3,1,4,5,6,7,8},
                {0,2,3,1,4,5,9}
        };
    }


    @Test
    public void testInputValue(){
        try{
            for (int i = 0; i < currentArray.length; i++) {
                inputNumbers.inputValues(currentArray[i]);
            }
        } catch (IndexOutOfBoundsException e){
            throw new IllegalArgumentException("array size incorrect");
        }

    }
}
