package input;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * this is my class
 * trying to sort some entered values
 */
public class InputNumbers {
    /**
     * I have been created variables scanner for input values and integerArrayList to store them
     * (cause It has not been defined how many values gonna be added)
     */
    static Scanner scanner = new Scanner(System.in);
    static ArrayList<Integer> integerArrayList = new ArrayList<>();
//    int counter = 0;
    public static void main(String[] args) {
        System.out.println("input numbers. if you want to stop, just press Enter");
        InputNumbers obj = new InputNumbers();
        /**
         * it's ok, if there will be 10 or less input values
         * in case of 0 input, loop will be close
         * otherwise, isValidInputString method will be called
         */
        while(integerArrayList.size()<10){
            String value = scanner.nextLine();
            if(value.isEmpty()){
                scanner.close();
                break;
            }
            else
                obj.isValidInputString(value);
        }
        /**
         * if no one number has been entered, sorting will not been called
         */
        if(integerArrayList.size()!=0)
            obj.sorting();
        System.out.println(integerArrayList.toString());

    }

    /**
     * method is trying to parse Integer value from String
     * calling inputValues method and return true, if successfully
     * throwing IllegalArgumentException, if not
     * @param s - entered String
     */
    public boolean isValidInputString(String s){
        boolean result;
        int a = 0;
        try{
            a = Integer.parseInt(s);
            result = true;
        } catch (NumberFormatException e){
            result = false;
            throw new NumberFormatException();
        }

        if(result)
            inputValues(a);
        return result;
    }

    /**
     * method is trying to add value to integerArrayList
     * if size of integerArrayList more 10, then IndexOutOfBoundsException will be called
     * @param a - parsed int value from entered String
     */
    public void inputValues(int a){
        if(integerArrayList.size()<10)
            integerArrayList.add(a);
        else
            throw new IndexOutOfBoundsException("too many values");
    }

    /**
     * simple bubble sorting method
     */
    public void sorting(){
        int max;
        for (int i = 0; i < integerArrayList.size(); i++) {
            for (int j = 0; j < integerArrayList.size()-1; j++) {
                if(integerArrayList.get(j+1)<integerArrayList.get(j)){
                    max = integerArrayList.get(j);
                    integerArrayList.set(j,integerArrayList.get(j+1));
                    integerArrayList.set(j+1,max);
                }
            }
        }
    }
}
